# Simple react demo

A simple react demo, same app as implemented with vue in 
https://gitlab.com/kjell/vuedemo

Its not usefull unless you also have https://gitlab.com/kjell/imgapi running.


## Build Setup

``` bash
# install dependencies
npm install

# Build
npm run-script build

# Serve current directory with some http server and visit
# http://localhost:8080/public/ 
```

