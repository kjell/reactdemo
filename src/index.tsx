import * as React from "react";
import * as  ReactDOM from "react-dom";
import { Gallery } from "./components/Gallery";

ReactDOM.render(
    <Gallery  webservice="http://localhost:8081/images" title="my gallery"/>,
    document.getElementById("gallery" ),
);
