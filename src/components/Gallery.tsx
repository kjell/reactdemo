import axios from "axios";
import * as React from "react";
import { Iimg, Image } from "./Image";

interface IGalleryProps {
    title: string;
    webservice: string;
}

interface IState {
    images: Iimg[];
    error: string; // se renderError()
}

export class Gallery extends React.Component<IGalleryProps, IState> {
    constructor(props: any) {
        super(props);
        this.getImages();
        this.state = { images: Array(), error: ""};
    }

    public render() {
        return <div>

        <div id="app">
          <div className="row">
            <h1>{ this.props.title } <i className="fas fa-hand-peace"></i></h1>
            { this.renderError()}
          </div>
          <div className="row">
            <div className="col-6 col">
              <pre>
from "{this.props.webservice}"<br />
{JSON.stringify(this.state.images, null, 2)}
              </pre>
            </div>
            <div className="col-6 col gallery">

    {   // list all images
        this.state.images.map((image, id) =>

         // foreach image, insert an instance of the image component.
         // Connect div handlers which handles state in parrent component (Gallery)

          <Image key={image.ID} image={image}
          webservice={this.props.webservice}
          likeHandler={() => this.likehandler(image.ID)}
          commentHandler={(c: string) => this.commenthandler(image.ID, c)}
          deleteHandler={() => this.deletehandler(image.ID)}
          />)
    }

            </div>
          </div>
        </div> {/* end #app */ }

        </div >
        ;
    }

    // Deletehandler removes image from gallery
    private deletehandler(id: number) {
        const i = this.state.images;
        delete i[id];
        this.setState({ images: i });
    }

    // Likehandler bumps likecount on image
    private likehandler(id: number) {
        const i = this.state.images;
        i[id].Likes++;
        this.setState({ images: i });
    }

    // commenthandler sets comment on image
    private commenthandler(id: number, comment: string) {
        const i = this.state.images;
        i[id].Comment = comment;
        this.setState({ images: i });
    }

    // renderError displays error (return html for allert defined in state.error)
    private renderError() {
        if (this.state.error !== "") {
        return <div className="alert alert-danger">{this.state.error}</div>;
        }
        return;
    }

    // ser ut som letteste måte å komme videre på er å servere en liste med bilder.
    // følgende er altså for å hente bildene fra api og transformere respons til  liste med Iimg[]
    private getImages() {
        axios.get(this.props.webservice).then((res) => {
            const i: Iimg[] = new Array();
            Object.keys(res.data).forEach((key) => {
                i.push(res.data[key]);
            });
            this.setState({images: i});
        }).catch((e) => {
            if (e.response) {
                this.setState({error: "error from service!:" + e.response.statusText});
            } else {
                this.setState({error: "could not fetch data!:" + e});
            }
        });
    }
}
