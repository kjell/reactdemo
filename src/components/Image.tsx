import * as React from "react";

export interface Iimg {
    ID: number;
    Name: string;
    RelPath: string;
    Likes: number;
    Comment: string;
}

interface IImageProps {
    image: Iimg;
    likeHandler: any;
    commentHandler: any;
    deleteHandler: any;
    webservice: string;
}

interface IImageState {
    editable: boolean;
    comment?: string;
}
export class Image extends React.Component<IImageProps, IImageState> {

    constructor(props: any) {
        super(props);
        this.state = {editable: false};
    }

    public render() {
        return <div className="card img" id={"img" + this.props.image.ID}>
        <h3><i className="fas fa-camera-retro"></i>{ this.props.image.Name }</h3>
        <img src={this.imageURL()} />

        <input disabled={!this.state.editable}
         value={this.state.comment}
         onChange={this.handleCommentChange.bind(this)}
         onKeyDown={ (e) => this.commentKeyDown(e)}
         />

        <div className="options">
          <a href="#" onClick={ (e) => this.like(e)}>
            <i className="fas fa-star" style={ {fontSize : this.getStarSize()}}></i>
          </a>
          <span className="badge success">{ this.props.image.Likes }</span>
          <a href="#" onClick={(e) => this.delete(e)}>
            <i className="fas fa-times"></i>
          </a>
          <a href="#" onClick={(e) => this.enableEdit(e)}>
            <i className="fas fa-edit"></i>
          </a>
        </div >
      </div >;
    }

    private commentKeyDown(e: any) {
        if (e.keyCode === 13) {
            this.enableEdit(e);
        }
    }

    private like(e: any) {
        e.preventDefault();
        this.props.likeHandler();
    }

    private delete(e: any) {
        e.preventDefault();
        this.props.deleteHandler();
    }

    private enableEdit(e: any) {
        e.preventDefault();
        this.setState({editable: !this.state.editable});
    }

    private getStarSize(): string {
     const fontsize = this.props.image.Likes < 3 ? this.props.image.Likes + 1 : 3;
     return fontsize + "em";
    }

    private handleCommentChange(e: any) {
        this.props.commentHandler(e.target.value);
    }

    private  imageURL(): string {
        return this.props.webservice + "/" + this.props.image.ID + "/img";
    }
}
